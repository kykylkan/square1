/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue').default;

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

import VueRouter from 'vue-router'
import ElementUI from 'element-ui';
import locale from 'element-ui/lib/locale/lang/en'
import 'element-ui/lib/theme-chalk/index.css';

Vue.use(VueRouter);
Vue.use(ElementUI, {locale});

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const routes = [
    {
        path: '/blog',
        name: 'blog.index',
        component: require('./blog/index').default,
        children: [
            {
                path: 'list',
                name: 'blog.list',
                component: require('./blog/list').default,
            },
            {
                path: 'create',
                name: 'blog.create',
                component: require('./blog/form').default,
            },
            {
                path: 'edit/:id',
                name: 'blog.edit',
                component: require('./blog/form').default,
            },
        ]
    }
];

const router = new VueRouter({
    routes
})

window.router = router;

const app = new Vue({
    router,
    el: '#app',
});
