<?php

namespace App\Services;

use App\Models\Post;
use App\Models\User;

/**
 * Class AbstractManager
 * @package App\Services
 */
abstract class AbstractManager
{
    /**
     * AbstractManager constructor.
     */
    public function __construct()
    {
    }

    /**
     * @param User $user
     * @param array $data
     * @return Post
     */
    public function createPost(User $user, array $data): Post
    {
        $data['user_id'] = $user->id;

        return Post::create($data);
    }
}
