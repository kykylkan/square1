<?php

namespace App\Http\Resources;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use JsonSerializable;

class PostResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param Request $request
     * @return array|Arrayable|JsonSerializable
     */
    public function toArray($request)
    {
        $canShowExtraFields = $request->route()->getName() == 'posts.index' || $request->user()->can('update', $this);

        return [
            'title' => $this->title,
            'description' => $this->description,
            'published_at' => $this->published_at->format('Y-m-d H:i'),

            $this->mergeWhen($canShowExtraFields, [
                'id' => $this->id,
                'created_at' => $this->created_at->format('Y-m-d H:i'),
            ]),
        ];
    }
}
