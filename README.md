Instructions

Requirements:
- PHP 8
- Node.js 12.14 or greater

I used sail for running this project:
- https://laravel.com/docs/8.x/installation#getting-started-on-macos

After project was cloned go to the root project folder - square1:
1. composer install
2. if .env will be not created: 
   - cp .env.example .env
3. php artisan key:generate
4. fill this settings in .env and in .env.testing:
   # only in .env
   - DB_PASSWORD=
   # common
   - SQ1_API_URL=
   - RECORDS_PER_PAGE=
5. ./vendor/bin/sail up -d
6. we should run any php command from docker container:
   - docker-compose exec laravel.test bash
7. php artisan migrate --seed
8. this command for running tests:
   - php artisan test
10. this command for getting posts from API:
    - php artisan sync:blog
    note 1: by default queues job will be started immediately with "sync" driver
    note 2: the best solution: you can change QUEUE_DRIVER to redis and run command php artisan queue:work after sync:blog
9. npm can be ran from root project folder:
   - npm i
   - npm run prod
11. open in the browser:
    - http://localhost/
