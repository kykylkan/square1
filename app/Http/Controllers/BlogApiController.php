<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostRequest;
use App\Http\Resources\PostResource;
use App\Models\Post;
use App\Repositories\Interfaces\BlogRepositoryInterface;
use App\Services\BlogManager;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class BlogApiController
 * @package App\Http\Controllers
 */
class BlogApiController extends Controller
{
    /**
     * @var BlogRepositoryInterface
     */
    private $blogRepository;

    /**
     * @var BlogManager
     */
    private $blogManager;

    /**
     * BlogApiController constructor.
     * @param BlogRepositoryInterface $blogRepository
     * @param BlogManager $blogManager
     */
    public function __construct(BlogRepositoryInterface $blogRepository, BlogManager $blogManager)
    {
        $this->middleware('can:view,post')->only(['edit']);
        $this->middleware('can:update,post')->only(['update']);
        $this->middleware('can:create,App\Models\Post')->only(['store']);
        $this->middleware('can:delete,post')->only(['destroy']);

        $this->blogRepository = $blogRepository;
        $this->blogManager = $blogManager;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $posts = $this->blogRepository->getUserPosts(auth()->user());

        return response()->json([
            'data' => PostResource::collection($posts)
        ], Response::HTTP_OK);
    }

    /**
     * @param PostRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(PostRequest $request)
    {
        $post = $this->blogManager->createPost(auth()->user(), $request->validated());

        return response()->json([
            'data' => new PostResource($post)
        ], Response::HTTP_CREATED);
    }

    /**
     * @param Post $post
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit(Post $post)
    {
//        $post = $this->blogRepository->getPostForEdit($post);

        return response()->json([
            'data' => new PostResource($post)
        ], Response::HTTP_OK);
    }


    /**
     * @param PostRequest $request
     * @param Post $post
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(PostRequest $request, Post $post)
    {
        $post = $this->blogManager->updatePost($post, $request->validated());

        return response()->json([], Response::HTTP_NO_CONTENT);
    }

    /**
     * @param Post $post
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Post $post)
    {
        $this->blogManager->deletePost($post);

        return response()->json([], Response::HTTP_NO_CONTENT);
    }
}
