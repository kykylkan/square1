<?php

namespace Tests\Unit;

use App\Jobs\SyncJob;
use App\Models\Post;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Pagination\Paginator;
use Tests\TestCase;
use Illuminate\Support\Facades\Bus;

class HomePageTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_user_can_view_a_home_page()
    {
        $this->actingAs($user = User::factory()->create());

        $anotherUser = User::factory()->create();
        $posts = Post::factory()->count(2)->create(['user_id' => $anotherUser->id]);
        $posts = new Paginator($posts, config('custom.per_page'));

        $view = $this->view('blog.index', ['posts' => $posts, 'sort' => 'desc']);

        $view->assertSee($posts->first()->title);
    }

    //TODO: need to check Cache
}
