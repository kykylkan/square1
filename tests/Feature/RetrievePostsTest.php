<?php

namespace Tests\Feature;

use App\Models\Post;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class RetrievePostsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_user_can_retrieve_public_posts()
    {
        $this->actingAs($user = User::factory()->create());

        $anotherUser = User::factory()->create();
        $posts = Post::factory()->count(2)->create(['user_id' => $anotherUser->id]);

        $response = $this->get('/');

        $response->assertStatus(200);
    }

    /** @test */
    public function a_user_can_only_retrieve_their_posts()
    {
        $this->actingAs($user = User::factory()->create());

        $anotherUser = User::factory()->create();
        $posts = Post::factory()->count(2)->create(['user_id' => $anotherUser->id]);

        $response = $this->get('/posts');

        $response->assertStatus(200)
            ->assertExactJson([
                'data' => [],
            ]);
    }
}
