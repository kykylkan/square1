<?php

namespace Tests\Feature;

use App\Models\Post;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UpdatePostsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_user_can_not_update_a_post()
    {
        $this->actingAs($user = User::factory()->create());
        $post = Post::factory()->create(['user_id' => $user->id]);

        $response = $this->put('/posts/'.$post->id, [
            'title' => 'Testing title2',
        ]);

        $response->assertStatus(403);
    }
}
